<?php
use Symfony\Component\HttpFoundation\Request;

// function get_client_ip() {
//     $ipaddress = '';
//     if (getenv('HTTP_CLIENT_IP'))
//         $ipaddress = getenv('HTTP_CLIENT_IP');
//     else if(getenv('HTTP_X_FORWARDED_FOR'))
//         $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
//     else if(getenv('HTTP_X_FORWARDED'))
//         $ipaddress = getenv('HTTP_X_FORWARDED');
//     else if(getenv('HTTP_FORWARDED_FOR'))
//         $ipaddress = getenv('HTTP_FORWARDED_FOR');
//     else if(getenv('HTTP_FORWARDED'))
//        $ipaddress = getenv('HTTP_FORWARDED');
//     else if(getenv('REMOTE_ADDR'))
//         $ipaddress = getenv('REMOTE_ADDR');
//     else
//         $ipaddress = 'UNKNOWN';
//     return $ipaddress;
// }

$review_controller = $app['controllers_factory'];
$review_controller->get('/', function (Request $request) use ($app) {
    $query = "SELECT * FROM review";
    $reviews = $app['db']->fetchAll($query);
    $userip = $request->getClientIp();
    $filtered_revs = array();
    foreach ($reviews as $rev) {
      $query = "SELECT * FROM likes WHERE id_review = ? AND ip = ?";
      $res = $app['db']->fetchAssoc($query, array((int) $rev['id'], (string) $userip));
      if($res == null || $res == '') {
      $rev['likes'] = 0;
      } else {
        $rev['likes'] = 1;
      }
      array_push($filtered_revs, $rev);
    }
      return $app['twig']->render('/index/index.html.twig',
          array('reviews' => $filtered_revs, 'userip' => $userip));
})->bind('homepage');

$review_controller->get('/filter/{type}', function ($type, Request $request) use ($app) {
  if(!$type) {
    $query = "SELECT * FROM review";
  } else {
    switch ($type) {
      case 'date_asc':
        $query = "SELECT * FROM review ORDER BY create_date ASC";
        break;
      case 'date_desc':
        $query = "SELECT * FROM review ORDER BY create_date DESC";

        break;
      case 'likes_asc':
        $query = "SELECT * FROM review ORDER BY review_likes ASC";

      break;
      case 'likes_desc':
        $query = "SELECT * FROM review ORDER BY review_likes DESC";
        break;
    }
  }
    $reviews = $app['db']->fetchAll($query);
    $userip = $request->getClientIp();
    $filtered_revs = array();
    foreach ($reviews as $rev) {
      $query = "SELECT * FROM likes WHERE id_review = ? AND ip = ?";
      $res = $app['db']->fetchAssoc($query, array((int) $rev['id'], (string) $userip));
      if($res == null || $res == '') {
      $rev['likes'] = 0;
      } else {
        $rev['likes'] = 1;
      }
      array_push($filtered_revs, $rev);
    }
      return $app['twig']->render('/index/index.html.twig',
          array('reviews' => $filtered_revs, 'userip' => $userip));



})->bind('homepage_filter');

$review_controller->get('/review/{id}', function ($id, Request $request) use ($app) {
  $userip = $request->getClientIp();
  $query = "SELECT * FROM review WHERE id=?";
  $review = $app['db']->fetchAssoc($query, array((int) $id));
  $previd = -1;
  $nextid = -1;
  if($review != '' && $review != null) {
    $query = "SELECT * FROM likes WHERE id_review = ? AND ip = ?";
    $res = $app['db']->fetchAssoc($query, array((int) $review['id'], (string) $userip));
    if($res == null || $res == '') {
    $review['likes'] = 0;
    } else {
      $review['likes'] = 1;
    }
    $ids_query = "SELECT id FROM review";
    $ids = $app['db']->fetchAll($ids_query);
    $filter_ids = array();
    foreach ($ids as $id_item) {
      array_push($filter_ids, $id_item['id']);
    }
    $curr_id = array_search($review['id'], $filter_ids);
    if($curr_id != -1) {
      if($curr_id != 0) {
        $previd = $filter_ids[$curr_id - 1];
      }
      if($curr_id < count($ids) - 1) {
        $nextid = $filter_ids[$curr_id + 1];
      }
    }

    return  $app['twig']->render('/review_item/review_item.html.twig',
  array('review' => $review, 'userip' => $userip, 'previd' => $previd, 'nextid' => $nextid));
  } else {
    return "Такого отзыва не существует";
  }
})->bind("review");

$review_controller->get('/api/check_ip', function(Request $request) use ($app) {
  $ip = $request->get('ip');
  $id = $request->get('id');
  $type = $request->get('type');
  if(!$ip || !$id || !$type) {
    return 'Fail';
  } else {
    if($type == 'add') {
      $query = "SELECT * FROM likes WHERE id_review = ? AND ip = ?";
      $res = $app['db']->fetchAssoc($query, array((int) $id, (string) $ip));
      if($res == null || $res == '') {
        $add_query = "INSERT INTO likes (id_review, ip) VALUES ( ?, ?)";
        $app['db']->executeUpdate($add_query, array((int) $id, (string) $ip));
        $likes_query = "SELECT * FROM review WHERE id = ?";
        $rev = $app['db']->fetchAssoc($likes_query, array((int) $id));
        $likes_count = $rev['review_likes'];
        // return $rev;
        ++$likes_count;
        $update_review_query = "UPDATE review SET review_likes = ? WHERE id = ?";
        $app['db']->executeUpdate($update_review_query, array((int) $likes_count, (int) $id));
        return 5;
      } else {
        return 3;
      }
    } else if($type == 'remove') {
      $query = "SELECT * FROM likes WHERE id_review = ? AND ip = ?";
      $res = $app['db']->fetchAssoc($query, array((int) $id, (string) $ip));
      if($res == null || $res == '') {
        return 5;
      } else {
        $remove_query = "DELETE FROM likes WHERE id_review= ? AND ip= ?";
        $app['db']->executeUpdate($remove_query, array((int) $id, (string) $ip));
        $likes_query = "SELECT * FROM review WHERE id = ?";
        $rev = $app['db']->fetchAssoc($likes_query, array((int) $id));
        $likes_count = $rev['review_likes'];
        --$likes_count;
        $update_review_query = "UPDATE review SET review_likes = ? WHERE id = ?";
        $app['db']->executeUpdate($update_review_query, array((int) $likes_count, (int) $id));
        return 3;
      }
    }
  }
});

$review_controller->match('/create_review',
function (Request $request) use ($app) {
  $form = $app['form.factory']->createBuilder('form')
        ->add('author_name')
        ->add('review_text', 'textarea')
        ->getForm();
   $form->handleRequest($request);
   if ($form->isSubmitted() && $form->isValid()) {
    $data = $form->getData();
    $author_ip = $request->getClientIp();
    $data['review_text'] = strip_tags($data['review_text']);
      $query = "INSERT INTO review (author_name, author_ip, review_text, review_likes)
        VALUES ('".$data['author_name']."', '".$author_ip."', '".$data['review_text']."', 0)";
        $ans = $app['db']->executeUpdate($query);
      return $app->redirect($app['url_generator']->generate('homepage'));
    }
  return $app['twig']->render('/create_review/create_review_form.html.twig',
    array('form' => $form->createView()));
})->bind('create_review');

return $review_controller;
