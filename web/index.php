<?php
require_once __DIR__.'/../vendor/autoload.php';
use Symfony\Component\HttpFoundation\Request;
use Silex\Provider\FormServiceProvider;

$app = new Silex\Application();

$app->mount('/', include __DIR__.'/../src/Controller/IndexController.php');

$app->register(new FormServiceProvider());

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../src/View'
));

$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'translator.domains' => array(),
));

$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
  'db.options' => array(
    'driver'    => 'pdo_mysql',
    'host'      => 'localhost',
    'dbname'    => 'guestbook',
    'user'      => 'root',
    'password'  => '',
    'charset'   => 'utf8',
  )
));
$app['debug'] = true;
// $app->get('/hello', function (Request $request) {
//   $bla = $request->get("name");
//   return 'пиздец '.$bla;
// });
// $app->get('/', function () {
//     return 'лололо';
// });
$app->run();
