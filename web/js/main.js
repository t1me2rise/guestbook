$( document ).ready(function() {
  $('.like_btn').on('click', function () {
    if($(this).attr('class') == 'glyphicon glyphicon-star-empty like_btn') {
      var like_button = $(this);
      var id = $(like_button).parent().attr('data-id');
      var ip = $(like_button).parent().attr('data-ip');
      $.ajax({
        url: '/api/check_ip',
        method: 'GET',
        data: {
          type: 'add',
          id: id,
          ip: ip
        },
        success: function (data) {
          if( parseInt(data) == 5) {
            $(like_button).removeClass('glyphicon glyphicon-star-empty like_btn').fadeOut(100);
            $(like_button).addClass('glyphicon glyphicon-star like_btn').fadeIn(300);
            var likes = $(like_button).parent().find('.review_likes');
            var likes_count = $(likes).text();
            ++likes_count;
            $(likes).text(likes_count);
          } else {
            console.log(data);
          }
        }
      });

    } else {
      var like_button = $(this);
      var id = $(like_button).parent().attr('data-id');
      var ip = $(like_button).parent().attr('data-ip');
      $.ajax({
        url: '/api/check_ip',
        method: 'GET',
        data: {
          type: 'remove',
          id: id,
          ip: ip
        },
        success: function (data) {
          if(data == 3) {
            $(like_button).removeClass('glyphicon glyphicon-star like_btn').fadeOut(100);
            $(like_button).addClass('glyphicon glyphicon-star-empty like_btn').fadeIn(300);
            var likes = $(like_button).parent().find('.review_likes');
            var likes_count = $(likes).text();
            --likes_count;
            $(likes).text(likes_count);
          } else {
            console.log(data);
          }
        }
      });
    }
  });
  $('#form_author_name').addClass('form-control');
  $('#form_review_text').addClass('form-control');
  $('input[name="submit"]').addClass('btn btn-primary');

});
